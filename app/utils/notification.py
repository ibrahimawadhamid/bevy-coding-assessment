import os
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from sqlalchemy.orm import Session
from .. import models, crud


def send_notification_via_gateway(
        db: Session, preference: models.preference.Preference, notification: models.notification.Notification):
    """Send a notification via the gateway (SMS/Email)."""
    if preference.notification_type == "sms" and preference.phone_number:
        try:
            account_sid = os.environ['TWILIO_ACCOUNT_SID']
            auth_token = os.environ['TWILIO_AUTH_TOKEN']
            from_number = os.environ['TWILIO_FROM_NUMBER']
            client = Client(account_sid, auth_token)
            client.messages.create(
                to=preference.phone_number,
                from_=from_number,
                body=notification.text)
            crud.notification.mark_notification_sent(db=db, notification=notification)
        except TwilioRestException as e:
            print(e)
    if preference.notification_type == "email" and preference.email:
        try:
            message = Mail(
                from_email=os.environ.get("FROM_EMAIL"),
                to_emails=preference.email,
                subject=notification.title,
                html_content=notification.text)
            send_grid_client = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))
            response = send_grid_client.send(message)
            if response.status_code == 202:
                crud.notification.mark_notification_sent(db=db, notification=notification)
        except Exception as e:
            print(e)
