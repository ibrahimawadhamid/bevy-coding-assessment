from fastapi import FastAPI
from . import config, routers, database


settings: config.Settings = config.get_settings()
app = FastAPI(
    title=settings.title, description=settings.description, version=settings.version,
    docs_url=settings.swagger, redoc_url=settings.redoc)
app.include_router(routers.home.router)
app.include_router(routers.preference.router)
app.include_router(routers.notification.router)


@app.on_event("startup")
def app_startup():
    """Create database connection on server startup."""
    database.create_db_connection()
