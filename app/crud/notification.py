from sqlalchemy.orm import Session
from .. import schemas, models


def get_notifications(
        db: Session, title: str = None, text: str = None,
        target_username: str = None, skip: int = 0, limit: int = 100):
    """Get all notification records from database paginated."""
    result = db.query(models.notification.Notification)
    if title is not None:
        result = result.filter(models.notification.Notification.title == title)
    if text is not None:
        result = result.filter(models.notification.Notification.text == text)
    if target_username is not None:
        result = result.filter(models.notification.Notification.target_username == target_username)
    return result.offset(skip).limit(limit).all()


def get_notification_by_id(db: Session, notification_id: int):
    """Get a single notification record from  database by id."""
    result = db.query(models.notification.Notification)\
        .filter(models.notification.Notification.id == notification_id).first()
    return result


def create_notification(db: Session, notification: schemas.notification.NotificationCreate):
    """Create a notification record in database."""
    db_notification = models.notification.Notification(
        title=notification.title,
        text=notification.text,
        target_username=notification.target_username,
        time=notification.time)
    db.add(db_notification)
    db.commit()
    db.refresh(db_notification)
    return db_notification


def mark_notification_sent(db: Session, notification: models.notification.Notification):
    """Set the flag 'sent' to True."""
    notification.sent = True
    db.commit()
    db.refresh(notification)


def delete_notification(db: Session, notification: models.notification.Notification):
    """Delete a single notification record from database."""
    db.delete(notification)
    db.commit()
    return notification
