from fastapi import HTTPException
from sqlalchemy.orm import Session
from .. import schemas, models


def get_preferences(
        db: Session, username: str = None, notification_type: str = None,
        phone_number: str = None, email: str = None, skip: int = 0, limit: int = 100):
    """Get all preferences records from database paginated."""
    result = db.query(models.preference.Preference)
    if username is not None:
        result = result.filter(models.preference.Preference.username == username)
    if notification_type is not None:
        result = result.filter(models.preference.Preference.notification_type == notification_type)
    if phone_number is not None:
        result = result.filter(models.preference.Preference.phone_number == phone_number)
    if email is not None:
        result = result.filter(models.preference.Preference.email == email)
    return result.offset(skip).limit(limit).all()


def get_preference_by_id(db: Session, preference_id: int):
    """Get a single preference record from  database by id."""
    result = db.query(models.preference.Preference)\
        .filter(models.preference.Preference.id == preference_id).first()
    return result


def get_preference_by_username(db: Session, username: str):
    """Get a single preference record from  database by username."""
    result = db.query(models.preference.Preference)\
        .filter(models.preference.Preference.username == username).first()
    return result


def create_preference(db: Session, preference: schemas.preference.PreferenceCreate):
    """Create a preference record in database."""
    db_preference = models.preference.Preference(
        username=preference.username,
        notification_type=preference.notification_type,
        phone_number=preference.phone_number,
        email=preference.email)
    db.add(db_preference)
    db.commit()
    db.refresh(db_preference)
    return db_preference


def update_preference(
        db: Session, original_preference: models.preference.Preference,
        updated_preference: schemas.preference.PreferenceUpdate):
    """Update a preference record in database with partial updates."""
    if updated_preference.notification_type:
        if updated_preference.notification_type == "sms":
            if not (updated_preference.phone_number or original_preference.phone_number):
                raise HTTPException(status_code=422, detail="No valid phone number found!")
        if updated_preference.notification_type == "email":
            if not (updated_preference.email or original_preference.email):
                raise HTTPException(status_code=422, detail="No valid email found!")
        original_preference.notification_type = updated_preference.notification_type
    if updated_preference.phone_number:
        original_preference.phone_number = updated_preference.phone_number
    if updated_preference.email:
        original_preference.email = updated_preference.email
    db.commit()
    db.refresh(original_preference)
    return original_preference


def delete_preference(db: Session, preference: models.preference.Preference):
    """Delete a single preference record from database."""
    db.delete(preference)
    db.commit()
    return preference
