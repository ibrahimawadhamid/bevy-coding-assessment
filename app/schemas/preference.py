import datetime
from typing import Optional
from enum import Enum
from pydantic import BaseModel, validator, root_validator, EmailStr
import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException


class NotificationType(str, Enum):
    """Enum for notification types."""

    sms = 'sms'
    email = 'email'
    none = 'none'


class PreferenceBase(BaseModel):
    """Base schema for a notification preference."""

    phone_number: Optional[str]
    email: Optional[EmailStr]

    @validator("phone_number")
    def phone_number_valid(cls, value):
        if value:
            try:
                number = phonenumbers.parse(value, None)
                if not phonenumbers.is_valid_number(number):
                    raise ValueError("Phone number is not valid")
            except NumberParseException as e:
                print(e)
                raise ValueError("Phone number is not valid")
        return value


class Preference(PreferenceBase):
    """Schema for an existing notification preference record."""

    id: int
    username: str
    notification_type: NotificationType
    phone_number: Optional[str]
    email: Optional[EmailStr]
    created_at: datetime.datetime
    updated_at: datetime.datetime

    class Config:
        orm_mode = True


class PreferenceCreate(PreferenceBase):
    """Schema for creating a new notification preference."""

    username: str
    notification_type: NotificationType

    @validator("username")
    def username_alphanumeric(cls, value):
        if value:
            assert value.isalnum(), "must be alphanumeric"
        return value

    @root_validator
    def notification_method(cls, values):
        if "notification_type" not in values:
            raise ValueError(f"notification_type must be one of {NotificationType}")
        if values["notification_type"] == "sms" and ("phone_number" not in values or not values["phone_number"]):
            raise ValueError("Phone number must exist for notification type 'sms'")
        if values["notification_type"] == "email" and ("email" not in values or not values["email"]):
            raise ValueError("Email must exist for notification type 'email'")
        return values


class PreferenceUpdate(PreferenceBase):
    notification_type: Optional[NotificationType]
    phone_number: Optional[str]
    email: Optional[EmailStr]
