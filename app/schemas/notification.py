import datetime
from pydantic import BaseModel


class NotificationBase(BaseModel):
    """Base schema for a notification."""

    title: str
    text: str
    target_username: str
    time: datetime.datetime = datetime.datetime.now()


class Notification(NotificationBase):
    """Schema for an existing notification record."""

    id: int
    sent: bool
    created_at: datetime.datetime
    updated_at: datetime.datetime

    class Config:
        orm_mode = True


class NotificationCreate(NotificationBase):
    """Schema for creating a new notification."""
    pass
