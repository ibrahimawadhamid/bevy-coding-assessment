from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from typing import List, Optional
from .. import crud, schemas, database

router = APIRouter(tags=["preferences"])


@router.get("/preferences/", response_model=List[schemas.preference.Preference])
def get_all_preferences(username: Optional[str] = None, notification_type: Optional[str] = None,
                        phone_number: Optional[str] = None, email: Optional[str] = None,
                        skip: int = 0, limit: int = 100, db: Session = Depends(database.get_db_session)):
    """Returns a list of all the preferences in the system."""
    preferences = crud.preference.get_preferences(
        db=db, username=username, notification_type=notification_type, phone_number=phone_number,
        email=email, skip=skip, limit=limit)
    return preferences


@router.get("/preferences/{username}/", response_model=schemas.preference.Preference)
def get_preference_by_username(username: str, db: Session = Depends(database.get_db_session)):
    """Returns a single preference filtered by username."""
    preference = crud.preference.get_preference_by_username(db=db, username=username)
    if preference is None:
        raise HTTPException(status_code=404, detail="Not found")
    return preference


@router.post("/preferences/", response_model=schemas.preference.Preference, status_code=201)
def create_preference(preference: schemas.preference.PreferenceCreate, db: Session = Depends(database.get_db_session)):
    """Create a single preference."""
    temp_preference = crud.preference.get_preference_by_username(db=db, username=preference.username)
    if temp_preference is not None:
        raise HTTPException(status_code=409, detail="Username already exists")
    return crud.preference.create_preference(db=db, preference=preference)


@router.patch("/preferences/{username}/", response_model=schemas.preference.Preference, status_code=200)
def update_preference(username: str, updated_preference: schemas.preference.PreferenceUpdate,
                      db: Session = Depends(database.get_db_session)):
    """Update a single preference attributes."""
    temp_preference = crud.preference.get_preference_by_username(db=db, username=username)
    if temp_preference is None:
        raise HTTPException(status_code=404, detail="Not found")
    result = crud.preference.update_preference(
        db=db, original_preference=temp_preference, updated_preference=updated_preference)
    return result


@router.delete("/preferences/{username}/", response_model=schemas.preference.Preference)
def delete_preference_by_username(username: str, db: Session = Depends(database.get_db_session)):
    """Delete a preference by username."""
    preference = crud.preference.get_preference_by_username(db=db, username=username)
    if preference is None:
        raise HTTPException(status_code=404, detail="Not found")
    return crud.preference.delete_preference(db=db, preference=preference)
