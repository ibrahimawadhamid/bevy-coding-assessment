from fastapi import APIRouter, Depends, HTTPException, BackgroundTasks
from sqlalchemy.orm import Session
from typing import List, Optional
from .. import crud, schemas, database, utils

router = APIRouter(tags=["notifications"])


@router.get("/notifications/", response_model=List[schemas.notification.Notification])
def get_all_notificationss(
        title: Optional[str] = None, text: Optional[str] = None, target_username: Optional[str] = None,
        skip: int = 0, limit: int = 100, db: Session = Depends(database.get_db_session)):
    """Returns a list of all notifications in the system."""
    notifications = crud.notification.get_notifications(
        db=db, title=title, text=text, target_username=target_username, skip=skip, limit=limit)
    return notifications


@router.get("/notifications/{notification_id}/", response_model=schemas.notification.Notification)
def get_preference_by_id(notification_id: int, db: Session = Depends(database.get_db_session)):
    """Returns a single notification filtered by id."""
    notification = crud.notification.get_notification_by_id(db=db, notification_id=notification_id)
    if notification is None:
        raise HTTPException(status_code=404, detail="Not found")
    return notification


@router.post("/notifications/", response_model=schemas.notification.Notification, status_code=201)
def create_notification(
        notification: schemas.notification.NotificationCreate, background_tasks: BackgroundTasks,
        db: Session = Depends(database.get_db_session)):
    """Create a single notification."""
    user_preference = crud.preference.get_preference_by_username(db=db, username=notification.target_username)
    if user_preference is None:
        raise HTTPException(status_code=404, detail="User not found")
    db_notification = crud.notification.create_notification(db=db, notification=notification)
    # send the notification in background
    background_tasks.add_task(utils.notification.send_notification_via_gateway, db=db, preference=user_preference,
                              notification=db_notification)
    return db_notification


@router.delete("/notifications/{notification_id}/", response_model=schemas.notification.Notification)
def delete_notification_by_id(notification_id: int, db: Session = Depends(database.get_db_session)):
    """Delete a notification by id."""
    notification = crud.notification.get_notification_by_id(db=db, notification_id=notification_id)
    if notification is None:
        raise HTTPException(status_code=404, detail="Not found")
    return crud.notification.delete_notification(db=db, notification=notification)
