from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime
from ..database import Base


class Preference(Base):
    """The database model for a notification preference."""
    __tablename__ = "preferences"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    notification_type = Column(String, default="None")
    phone_number = Column(String, nullable=True)
    email = Column(String, nullable=True)
    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
