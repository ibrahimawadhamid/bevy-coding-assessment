from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.sql.sqltypes import Boolean
from ..database import Base


class Notification(Base):
    """The database model for a notification."""
    __tablename__ = "notifications"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    text = Column(String)
    target_username = Column(String)
    sent = Column(Boolean, default=False)
    time = Column(DateTime, default=datetime.now)
    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
