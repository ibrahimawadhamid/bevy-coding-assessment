import os
from collections import namedtuple
import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.pool import StaticPool
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database
from .. import main, models, database


def create_db():
    """Create testing sqlite database."""
    engine = create_engine("sqlite:///./test.db", connect_args={"check_same_thread": False}, poolclass=StaticPool)
    testing_session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    if not database_exists(engine.url):
        create_database(engine.url)
    else:
        engine.connect()
    database.Base.metadata.create_all(bind=engine)
    return testing_session


def get_testing_db_session():
    """Get the DB session object."""
    session = create_db()
    db = session()
    try:
        yield db
    finally:
        db.close()


# override the normal app db_session with the testing one.
main.app.dependency_overrides[database.get_db_session] = get_testing_db_session

client = TestClient(main.app)


PreferenceRecord = namedtuple("PreferenceRecord", ["username", "notification_type", "phone_number", "email"])
PREFERENCE_RECORD = PreferenceRecord(
    username="username",
    notification_type="email",
    phone_number="+201003138613",
    email="ibrahim.a.hamid@gmail.com"
)

NotificationRecord = namedtuple("NotificationRecord", ["title", "text", "target_username"])
NOTIFICATION_RECORD = NotificationRecord(
    title="title",
    text="text",
    target_username=PREFERENCE_RECORD.username
)


@pytest.fixture
def create_notification_record():
    """A fixture to create a notification record."""
    prefrence_record = models.preference.Preference(
        username=PREFERENCE_RECORD.username,
        notification_type=PREFERENCE_RECORD.notification_type,
        phone_number=PREFERENCE_RECORD.phone_number,
        email=PREFERENCE_RECORD.email
    )
    notification_record = models.notification.Notification(
        title=NOTIFICATION_RECORD.title,
        text=NOTIFICATION_RECORD.text,
        target_username=NOTIFICATION_RECORD.target_username
    )
    db = list(get_testing_db_session())[0]
    db.add(prefrence_record)
    db.add(notification_record)
    db.commit()
    db.refresh(prefrence_record)
    db.refresh(notification_record)
    return notification_record


def test_create_notification_success(db_fixture):
    """Test create a notification record."""
    response = client.post(
        "/preferences/",
        json={
            "username": PREFERENCE_RECORD.username,
            "notification_type": PREFERENCE_RECORD.notification_type,
            "phone_number": PREFERENCE_RECORD.phone_number,
            "email": PREFERENCE_RECORD.email
        }
    )
    response = client.post(
        "/notifications/",
        json={
            "title": NOTIFICATION_RECORD.title,
            "text": NOTIFICATION_RECORD.text,
            "target_username": NOTIFICATION_RECORD.target_username
        }
    )
    assert response.status_code == 201
    assert response.json()['id']


def test_create_notification_failure(db_fixture):
    """Test fail to create a notification record."""
    response = client.post("/notifications/")
    assert response.status_code == 422


def test_get_all_notifications(db_fixture, create_notification_record):
    """Test get all notification records."""
    response = client.get("/notifications/")
    assert response.status_code == 200
    assert len(response.json()) > 0
    target_records = [item for item in response.json() if item['id'] == create_notification_record.id]
    assert len(target_records) == 1


def test_get_single_notification(db_fixture, create_notification_record):
    """Test get a single notification record."""
    response = client.get(f"/notifications/{create_notification_record.id}/")
    assert response.status_code == 200
    assert create_notification_record.id == response.json()['id']


def test_get_single_notification_not_exists(db_fixture):
    """Test get a single notification record that doesn't exist."""
    response = client.get("/notifications/1000/")
    assert response.status_code == 404


def test_delete_single_notification_exists(db_fixture, create_notification_record):
    """Test delete a single notification record that exists."""
    response = client.delete(f"/notifications/{create_notification_record.id}/")
    assert response.status_code == 200
    assert create_notification_record.id == response.json()['id']


def test_delete_single_notification_not_exists(db_fixture):
    """Test delete a single notification record that exists."""
    response = client.delete("/notifications/1000/")
    assert response.status_code == 404


@pytest.fixture(autouse=False)
def db_fixture():
    # prepare database
    create_db()
    yield None
    # remove database for cleanup
    os.remove('test.db')
