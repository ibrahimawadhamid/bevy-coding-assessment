import os
from collections import namedtuple
import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.pool import StaticPool
from sqlalchemy.orm import sessionmaker
from sqlalchemy_utils import database_exists, create_database
from .. import main, models, database


def create_db():
    """Create testing sqlite database."""
    engine = create_engine("sqlite:///./test.db", connect_args={"check_same_thread": False}, poolclass=StaticPool)
    testing_session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    if not database_exists(engine.url):
        create_database(engine.url)
    else:
        engine.connect()
    database.Base.metadata.create_all(bind=engine)
    return testing_session


def get_testing_db_session():
    """Get the DB session object."""
    session = create_db()
    db = session()
    try:
        yield db
    finally:
        db.close()


# override the normal app db_session with the testing one.
main.app.dependency_overrides[database.get_db_session] = get_testing_db_session

client = TestClient(main.app)


PreferenceRecord = namedtuple("PreferenceRecord", ["username", "notification_type", "phone_number", "email"])
PREFERENCE_RECORD = PreferenceRecord(
    username="username",
    notification_type="email",
    phone_number="+201003138613",
    email="ibrahim.a.hamid@gmail.com"
)


@pytest.fixture
def create_preference_record():
    """A fixture to create a preference record."""
    prefrence_record = models.preference.Preference(
        username=PREFERENCE_RECORD.username,
        notification_type=PREFERENCE_RECORD.notification_type,
        phone_number=PREFERENCE_RECORD.phone_number,
        email=PREFERENCE_RECORD.email
    )
    db = list(get_testing_db_session())[0]
    db.add(prefrence_record)
    db.commit()
    db.refresh(prefrence_record)
    return prefrence_record


def test_create_preference_success(db_fixture):
    """Test create a preference record."""
    response = client.post(
        "/preferences/",
        json={
            "username": PREFERENCE_RECORD.username,
            "notification_type": PREFERENCE_RECORD.notification_type,
            "phone_number": PREFERENCE_RECORD.phone_number,
            "email": PREFERENCE_RECORD.email
        }
    )
    assert response.status_code == 201
    assert response.json()['id']


def test_create_preference_failure(db_fixture):
    """Test fail to create a preference record."""
    response = client.post("/preferences/")
    assert response.status_code == 422


def test_get_all_preferences(db_fixture, create_preference_record):
    """Test get all preferences records."""
    response = client.get("/preferences/")
    assert response.status_code == 200
    assert len(response.json()) > 0
    target_records = [item for item in response.json() if item['id'] == create_preference_record.id]
    assert len(target_records) == 1


def test_get_single_preference(db_fixture, create_preference_record):
    """Test get a single preference record."""
    response = client.get(f"/preferences/{create_preference_record.username}/")
    assert response.status_code == 200
    assert create_preference_record.username == response.json()['username']


def test_get_single_preference_not_exists(db_fixture):
    """Test get a single preference record that doesn't exist."""
    response = client.get("/preferences/NON_EXISTING_USERNAME/")
    assert response.status_code == 404


def test_update_preference_success(db_fixture, create_preference_record):
    """Test update a preference record."""
    response = client.patch(
        f"/preferences/{create_preference_record.username}/",
        json={
            "notification_type": "sms"
        }
    )
    assert response.status_code == 200
    assert response.json()['notification_type'] == "sms"
    assert response.json()['phone_number'] == "+201003138613"


def test_update_preference_failure(db_fixture, create_preference_record):
    """Test fail to update a preference record."""
    response = client.patch(
        f"/preferences/{create_preference_record.username}/",
        json={
            "notification_type": "INVALID_TYPE"
        }
    )
    assert response.status_code == 422


def test_delete_single_preference_exists(db_fixture, create_preference_record):
    """Test delete a single preference record that exists."""
    response = client.delete(f"/preferences/{create_preference_record.username}/")
    assert response.status_code == 200
    assert create_preference_record.username == response.json()['username']


def test_delete_single_preference_not_exists(db_fixture):
    """Test delete a single preference record that exists."""
    response = client.delete("/preferences/NON_EXISTING_USERNAME/")
    assert response.status_code == 404


@pytest.fixture(autouse=False)
def db_fixture():
    # prepare database
    create_db()
    yield None
    # remove database for cleanup
    os.remove('test.db')
