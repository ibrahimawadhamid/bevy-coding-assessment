import os
from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


username = os.getenv("POSTGRES_USER")
password = os.getenv("POSTGRES_PASSWORD")
host = os.getenv("POSTGRES_HOST")
port = os.getenv("POSTGRES_PORT")
db_name = os.getenv("POSTGRES_DB")
database_url = "postgresql://{username}:{password}@{host}:{port}/{db_name}".format(
    username=username, password=password, host=host, port=port, db_name=db_name)

if [x for x in (username, password, host, port, db_name) if x is None or not x]:
    # if any of the parameters is none, fallback to sqllite database
    database_url = "sqlite:///./bevy-coding-assessment.db"


engine = create_engine(database_url)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db_session():
    """Get the DB session object."""
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def create_db_connection():
    """Create database if it does not exist and connect to it."""
    if not database_exists(engine.url):
        create_database(engine.url)
    else:
        engine.connect()
    Base.metadata.create_all(bind=engine)
