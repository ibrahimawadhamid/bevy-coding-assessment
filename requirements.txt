# core dependencies
fastapi==0.68.1
uvicorn==0.15.0
requests==2.26.0
python-dotenv==0.19.0
async-exit-stack==1.0.1
async-generator==1.10
# SMS
twilio==6.63.2
phonenumbers==8.12.31
# Email
sendgrid==6.8.1
email-validator==1.1.3
# database
SQLAlchemy==1.4.23
SQLAlchemy-Utils==0.37.8
psycopg2==2.9.1
# lint
flake8==3.9.2
# format
autopep8==1.5.7
# testing
pytest==6.2.5
# git pre-commit
pre-commit==2.15.0