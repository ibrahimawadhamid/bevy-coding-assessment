# Bevy Coding Assessment

A solution to the Bevy coding assessment task.

Online demo at [notification-manager.mintacube.com](https://notification-manager.mintacube.com/)

# Index

- [Introduction](#introduction)
- [Development](#development)
- [API](#api)
- [Local Docker Deployment](#local-docker-deployment)
- [Production Docker Deployment](#production-docker-deployment)
- [CI/CD](#cicd)
- [Code Formatting](#code-formatting)
- [Code Linting](#code-linting)
- [Code Testing](#code-testing)

# Introduction

- This service can be used as a standalone, but in a real world scenario, it would be used as a part of an eco-system.
- It handles notification preferences.
- The service can use `POSTGRESQL`, but will fall back to `SQL-Lite` if none is provided, for the sake of this task, you are encouraged to not provide any `POSTGRESQL` settings and just let it use the default `SQL-Lite` database.

## Development

- clone repo and setup virtualenvironment

  ```
  git clone git@gitlab.com:ibrahimawadhamid/bevy-coding-assessment.git
  cd bevy-coding-assessment
  virtualenv venv
  source venv/bin/activate
  pip install -r requirements.txt
  ```

- rename `.env.example` to `.env` and change the parameters as needed

- start server at [localhost:8000](http://localhost:8000)

  ```
  uvicorn app.main:app --reload --host 0.0.0.0 --port 8000
  ```

## API

- The easyest way to get started with the API is to use the **swagger** playground available at [http://localhost:8000/docs](http://localhost:8000/docs).

- An OpenAPI specifications JSON is availble at [http://localhost:8000/openapi.json](http://localhost:8000/openapi.json).

- A Postman collection file is available on this repo with the name `api.postman_collection.json`, import directly and use in Postman.

The following are the available API endpoints with their methods:

| URL                               | Method | Description                     |
|-----------------------------------|--------|---------------------------------|
| /                                 | GET    | Service Home                    |
| /preferences/                     | GET    | Get all preferences             |
| /preferences/                     | POST   | Create a preference             |
| /preferences/{username}/          | GET    | Get a preference by username    |
| /preferences/{username}/          | DELETE | Delete a preference by username |
| /preferences/{username}/          | PATCH  | Update a preference attributes  |
| /notifications/                   | GET    | Get all notifications           |
| /notifications/                   | POST   | Create a notification           |
| /notifications/{notification_id}/ | GET    | Get a notification by id        |
| /notifications/{notification_id}/ | DELETE | Delete a notification by id     |

### Examples:

- Create a preference
  ```
  curl -X 'POST' \
    'http://localhost:8000/preferences/' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{
    "phone_number": "+201003138613",
    "email": "ibrahim.a.hamid@gmail.com",
    "username": "ibrahim",
    "notification_type": "sms"
  }'
  ```

- Update a preference
  ```
  curl -X 'PATCH' \
    'http://localhost:8000/preferences/ibrahim/' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{
    "notification_type": "email"
  }'
  ```

- Create a notification
  ```
  curl -X 'POST' \
    'http://localhost:8000/notifications/' \
    -H 'accept: application/json' \
    -H 'Content-Type: application/json' \
    -d '{
    "title": "Test",
    "text": "Hello World!",
    "target_username": "ibrahim",
    "time": "2021-09-21T16:19:44.212584"
  }'
  ```

## Local Docker Deployment

The following repo was used to create the docker file
[https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker](https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker)

To build the image locally run the following command

```
docker build -t bevy-coding-assessment .
```

To run the image locally at [localhost:8000](http://localhost:8000) use the following command

```
docker run -d --name bevy-coding-assessment -p 8000:80 bevy-coding-assessment
```

## Production Docker Deployment

A `docker-compose.yml` file is available on this repo which should work as easy as running `docker-compose up -d`.

Environment variables might be required for the service to run properly, so make sure to modify the `docker-compose.yml` file per your needs.

Always use a docker image with a version tag and not use the **latest** tag. example: `registry.gitlab.com/ibrahimawadhamid/bevy-coding-assessment:0.1.0`. Tags on this repo map to a docker tag.

## CI/CD

`gitlab` is used for `CI/CD`. All configurations are included in the `gitlab-ci.yml`.

## Code Formatting

`autopep8` is used for code formatting. You can run it at any time using the following command.

```
autopep8 --in-place --recursive ./app
```

## Code Linting

`flake8` is used for code linting. You can run it at any time using the following command.

```
flake8
```

## Code Testing

`pytest` is used for unit testing. You can run it at any time using the following command.

```
pytest
```